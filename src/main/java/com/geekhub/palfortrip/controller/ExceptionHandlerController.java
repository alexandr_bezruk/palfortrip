package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.exception.NotFoundException;
import com.geekhub.palfortrip.exception.ServerErrorException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Controller
public class ExceptionHandlerController {

    @ExceptionHandler(NotFoundException.class)
    public String notFound() {
        return "/error/404";
    }

    @ExceptionHandler(ServerErrorException.class)
    public String serverError() {
        return "/error/500";
    }
}
