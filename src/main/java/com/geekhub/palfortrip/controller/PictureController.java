package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.service.PictureService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/images")
public class PictureController {

    private PictureService pictureService;

    public PictureController(PictureService pictureService) {
        this.pictureService = pictureService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/{username}")
    public byte[] getImage(@PathVariable String username) {
        return pictureService.getProfilePictureForUser(username);
    }
}
