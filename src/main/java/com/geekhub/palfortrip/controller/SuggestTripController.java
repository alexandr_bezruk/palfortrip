package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.dto.TripSuggestionForm;
import com.geekhub.palfortrip.service.TripService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/suggest")
public class SuggestTripController {

    private TripService tripService;

    public SuggestTripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping
    String getSuggestTemplate(TripSuggestionForm form, ModelMap model) {
        model.addAttribute("form", form);
        return "suggest_trip";
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping
    String suggest(@Valid @ModelAttribute("form") TripSuggestionForm form, BindingResult result) {
        if (result.hasErrors()) {
            return "suggest_trip";
        }
        Integer tripId = tripService.saveNewTrip(form);
        return "redirect:/trip/" + tripId;
    }
}
