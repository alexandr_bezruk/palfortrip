package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.dto.FeedbackForm;
import com.geekhub.palfortrip.service.FeedbackService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/feedback")
public class FeedbackController {

    private FeedbackService feedbackService;

    public FeedbackController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/{username}")
    String saveFeedback(FeedbackForm form, @PathVariable String username) {
        feedbackService.saveFeedback(form);
        return "redirect:/profile/"+username;
    }
}
