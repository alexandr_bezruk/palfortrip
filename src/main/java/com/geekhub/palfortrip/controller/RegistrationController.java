package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.dto.RegistrationForm;
import com.geekhub.palfortrip.model.User;
import com.geekhub.palfortrip.service.UserService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@RequestMapping("/registration")
@Controller
public class RegistrationController {

    private UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping
    String registration(RegistrationForm form, ModelMap model) {
        model.addAttribute("form", form);
        return "registration";
    }

    @PreAuthorize("permitAll()")
    @PostMapping
    String register(@Valid @ModelAttribute("form") RegistrationForm form, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            Boolean userExists = userService.userExists(form.getUsername());
            model.addAttribute("loginTaken", userExists);
            return "registration";
        }
        userService.saveUser(form);
        return "redirect:/login";
    }
}
