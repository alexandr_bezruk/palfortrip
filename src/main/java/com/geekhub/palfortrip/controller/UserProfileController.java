package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.dto.UserProfile;
import com.geekhub.palfortrip.dto.UserProfileEditForm;
import com.geekhub.palfortrip.dto.Feedback;
import com.geekhub.palfortrip.dto.UsersTrip;
import com.geekhub.palfortrip.model.TripStatus;
import com.geekhub.palfortrip.service.FeedbackService;
import com.geekhub.palfortrip.service.TripService;
import com.geekhub.palfortrip.service.UserProfileService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/profile")
public class UserProfileController {

    private UserProfileService userProfileService;
    private FeedbackService feedbackService;
    private TripService tripService;

    public UserProfileController(UserProfileService userProfileService, FeedbackService feedbackService, TripService tripService) {
        this.userProfileService = userProfileService;
        this.feedbackService = feedbackService;
        this.tripService = tripService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping(value = "/{username}")
    String getProfile(@PathVariable String username, Model model) {
        UserProfile userProfile = userProfileService.findUserProfileByUsername(username);
        List<Feedback> feedbacks = feedbackService.getFeedbacksForUser(username);
        Map<String, List<UsersTrip>> usersTripsByStatus = tripService.getTripsByUsernameGroupByStatus(username);
        List<UsersTrip> activeTrips = usersTripsByStatus.get(TripStatus.ACTIVE.toString());
        List<UsersTrip> completedTrips = usersTripsByStatus.get(TripStatus.COMPLETED.toString());
        model.addAttribute("userProfile", userProfile);
        model.addAttribute("feedbacks", feedbacks);
        model.addAttribute("activeTrips", activeTrips);
        model.addAttribute("completedTrips", completedTrips);
        return "profile";
    }

    @PreAuthorize("#username == authentication.name")
    @GetMapping(value = "/{username}/edit")
    String editProfile(@PathVariable String username, UserProfileEditForm form, ModelMap model) {
        UserProfile userProfile = userProfileService.findUserProfileByUsername(username);
        model.addAttribute("userProfile", userProfile);
        model.addAttribute("form", form);
        return "edit_profile";
    }

    @PostMapping(value = "/{username}/edit")
    String editProfilePost(@Valid @ModelAttribute("form") UserProfileEditForm form, BindingResult result,
                           ModelMap model, @PathVariable("username") String username,
                           @RequestPart MultipartFile profilePicture) {
        if (result.hasErrors()) {
            UserProfile userProfile = userProfileService.findUserProfileByUsername(username);
            model.addAttribute("userProfile", userProfile);
//            return "redirect:/profile/" + form.getUsername() + "/edit";
            return "edit_profile";
        }
        userProfileService.updateUserProfile(form, profilePicture);
        return "redirect:/profile/" + form.getUsername();
    }
}
