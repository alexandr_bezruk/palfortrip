package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.dto.City;
import com.geekhub.palfortrip.service.TripService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CityController {

    private TripService tripService;

    public CityController(TripService tripService) {
        this.tripService = tripService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping(value = "/findCity")
    public List<City> findCitiesStartsFrom(@RequestParam("term") String string) {
        return tripService.findCitiesStartsFrom(string);
    }
}
