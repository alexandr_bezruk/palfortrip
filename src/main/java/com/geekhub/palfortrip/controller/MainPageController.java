package com.geekhub.palfortrip.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/")
@Controller
public class MainPageController {

    @PreAuthorize("permitAll()")
    @GetMapping
    String indexPage() {
        return "index";
    }
}
