package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.service.FileService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Controller
@RequestMapping("/trips/{username}")
public class FileController {

    private FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(value = "/excel")
    public void getExcelDocument(HttpServletResponse response, @PathVariable("username") String username) {
        HSSFWorkbook excelWorkbook = fileService.createExcelTripsDocumentForUser(username);

        response.setHeader("Content-disposition", "attachment; filename=trips" + username + ".xls");
        try (OutputStream out = new BufferedOutputStream(response.getOutputStream())) {
            excelWorkbook.write(out);
        } catch (IOException e) {
            throw new RuntimeException("Error during writing file to output stream", e);
        }
    }

    @GetMapping(value = "/pdf")
    public void getPdfDocument(HttpServletResponse response, @PathVariable("username") String username) {
        Document pdfDocument = new Document();
        try {
            PdfWriter.getInstance(pdfDocument, response.getOutputStream());
            pdfDocument.open();
            fileService.writeTripsToTripsPdfDocumentForUser(pdfDocument, username);
            pdfDocument.close();
        } catch (IOException e) {
            throw new RuntimeException("Error during writing file to output stream", e);
        } catch (DocumentException e) {
            throw new RuntimeException("Error during pdf document creation", e);
        }
    }

    @GetMapping(value = "/word")
    public void getWordDocument(HttpServletResponse response, @PathVariable("username") String username) {
        XWPFDocument wordDocument = fileService.createTripsWordDocumentForUser(username);

        response.setHeader("Content-disposition", "attachment; filename=trips" + username + ".doc");
        try (OutputStream out = new BufferedOutputStream(response.getOutputStream())) {
            wordDocument.write(out);
        } catch (IOException e) {
            throw new RuntimeException("Error during writing file to output stream", e);
        }
    }
}
