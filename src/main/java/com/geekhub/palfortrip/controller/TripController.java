package com.geekhub.palfortrip.controller;

import com.geekhub.palfortrip.dto.FindTripForm;
import com.geekhub.palfortrip.dto.TripDetails;
import com.geekhub.palfortrip.dto.TripDto;
import com.geekhub.palfortrip.service.TripService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TripController {

    private TripService tripService;

    public TripController(TripService tripService) {
        this.tripService = tripService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/find")
    String findTripPage() {
        return "find_trip";
    }

    @PreAuthorize("permitAll()")
    @PostMapping("/find")
    String findTrip(@Valid FindTripForm form, BindingResult result, Model model) {
        if (result.hasErrors()) {
            List<ObjectError> allErrors = result.getAllErrors();
            model.addAttribute("validationErrors", allErrors);
            return "find_trip";
        }

        List<TripDto> trips = tripService.findTrips(form);
        model.addAttribute("form", form);
        model.addAttribute("trips", trips);
        return "trips";
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/trip/{id}")
    String tripDetails(@PathVariable Integer id, Model model) {
        TripDetails trip = tripService.getTripById(id);
        model.addAttribute("trip", trip);
        return "trip_details";
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/trip/{id}/complete")
    String completeTrip(@PathVariable("id") Integer tripId) {
        tripService.completeTrip(tripId);
        String currentUserUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        return "redirect:/profile/" + currentUserUsername;
    }

    @PreAuthorize("hasAuthority('USER')")
    @PostMapping("/trip/{id}/cancel")
    String cancelTrip(@PathVariable("id") Integer tripId) {
        tripService.cancelTrip(tripId);
        String currentUserUsername = SecurityContextHolder.getContext().getAuthentication().getName();
        return "redirect:/profile/" + currentUserUsername;
    }
}
