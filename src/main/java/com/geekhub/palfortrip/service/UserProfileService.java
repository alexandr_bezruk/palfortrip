package com.geekhub.palfortrip.service;

import com.geekhub.palfortrip.dto.UserProfile;
import com.geekhub.palfortrip.dto.UserProfileEditForm;
import com.geekhub.palfortrip.repository.feedback.FeedbackRepository;
import com.geekhub.palfortrip.repository.user.UserRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Repository
public class UserProfileService {

    private UserRepository userRepository;
    private FeedbackRepository feedbackRepository;
    private PictureService pictureService;

    public UserProfileService(UserRepository userRepository, FeedbackRepository feedbackRepository, PictureService pictureService) {
        this.userRepository = userRepository;
        this.feedbackRepository = feedbackRepository;
        this.pictureService = pictureService;
    }

    @Transactional
    public void updateUserProfile(UserProfileEditForm form, MultipartFile profilePicture) {
        Boolean userHasProfilePicture = pictureService.userHasProfilePicture(form.getUsername());
        if (!profilePicture.isEmpty() && userHasProfilePicture) {
            pictureService.updatePicture(profilePicture, form.getUsername());
            Integer pictureId = pictureService.getProfilePictureId(form.getUsername());
            form.setPictureId(pictureId);
        } else if (!profilePicture.isEmpty() && !userHasProfilePicture) {
            pictureService.saveProfilePicture(profilePicture, form.getUsername());
            Integer pictureId = pictureService.getProfilePictureId(form.getUsername());
            form.setPictureId(pictureId);
        }
        userRepository.updateUser(form);
    }

    @Transactional
    public UserProfile findUserProfileByUsername(String username) {
        UserProfile userProfile = userRepository.findUserProfileByUsername(username);
        userProfile.setAverageRating(feedbackRepository.getAverageRatingForUser(username));

        return userProfile;
    }
}
