package com.geekhub.palfortrip.service;

import com.geekhub.palfortrip.repository.picture.PictureRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class PictureService {

    private PictureRepository pictureRepository;

    public PictureService(PictureRepository pictureRepository) {
        this.pictureRepository = pictureRepository;
    }

    public void saveProfilePicture(MultipartFile profilePicture, String username) {
        pictureRepository.savePictureForUser(profilePicture, username);
    }

    public byte[] getProfilePictureForUser(String username) {
        return pictureRepository.getProfilePictureForUser(username);
    }

    public Boolean userHasProfilePicture(String username) {
        return pictureRepository.userHasProfilePicture(username);
    }

    public void updatePicture(MultipartFile picture, String username) {
        pictureRepository.updateProfilePicture(picture, username);
    }

    public Integer getProfilePictureId(String username) {
        return pictureRepository.getProfilePictureId(username);
    }
}
