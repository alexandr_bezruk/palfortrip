package com.geekhub.palfortrip.service;

import com.geekhub.palfortrip.dto.Feedback;
import com.geekhub.palfortrip.dto.UsersTrip;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class FileService {

    private TripService tripService;

    public FileService(TripService tripService) {
        this.tripService = tripService;
    }

    public HSSFWorkbook createExcelTripsDocumentForUser(String username) {
        HSSFWorkbook excelWorkbook = new HSSFWorkbook();
        HSSFSheet sheet = excelWorkbook.createSheet("FirstSheet");

        HSSFRow rowHead = sheet.createRow((short) 0);
        rowHead.createCell(0).setCellValue("Id");
        rowHead.createCell(1).setCellValue("Origin City");
        rowHead.createCell(2).setCellValue("Origin State");
        rowHead.createCell(3).setCellValue("Destination City");
        rowHead.createCell(4).setCellValue("Destination State");
        rowHead.createCell(5).setCellValue("Status");
        rowHead.createCell(6).setCellValue("Cost");
        rowHead.createCell(7).setCellValue("Date");

        Map<String, List<UsersTrip>> userTripsByStatus = tripService.getTripsByUsernameGroupByStatus(username);
        List<UsersTrip> activeTrips = userTripsByStatus.get("ACTIVE");
        short rowCount = createRowsInExcelDocument(sheet, 1, activeTrips);
        List<UsersTrip> completedTrips = userTripsByStatus.get("COMPLETED");
        createRowsInExcelDocument(sheet, rowCount, completedTrips);

        return excelWorkbook;
    }

    public void writeTripsToTripsPdfDocumentForUser(Document pdfDocument, String username) throws DocumentException {
        Map<String, List<UsersTrip>> userTripsByStatus = tripService.getTripsByUsernameGroupByStatus(username);
        List<UsersTrip> activeTrips = userTripsByStatus.get("ACTIVE");
        writeTripsToPdfDocument(activeTrips, pdfDocument);
        List<UsersTrip> completedTrips = userTripsByStatus.get("COMPLETED");
        writeTripsToPdfDocument(completedTrips, pdfDocument);
    }

    public XWPFDocument createTripsWordDocumentForUser(String username) {
        XWPFDocument wordDocument = new XWPFDocument();

        Map<String, List<UsersTrip>> userTripsByStatus = tripService.getTripsByUsernameGroupByStatus(username);
        List<UsersTrip> activeTrips = userTripsByStatus.get("ACTIVE");
        writeLinesToWordDocument(wordDocument, activeTrips);
        List<UsersTrip> completedTrips = userTripsByStatus.get("COMPLETED");
        writeLinesToWordDocument(wordDocument, completedTrips);

        return wordDocument;
    }

    private short createRowsInExcelDocument(HSSFSheet sheet, int rowCount, List<UsersTrip> trips) {
        short count = (short) rowCount;
        for (UsersTrip trip : trips) {
            HSSFRow row = sheet.createRow(count);
            row.createCell(0).setCellValue(trip.getId());
            row.createCell(1).setCellValue(trip.getOrigin().getCity());
            row.createCell(2).setCellValue(trip.getOrigin().getState());
            row.createCell(3).setCellValue(trip.getDestination().getCity());
            row.createCell(4).setCellValue(trip.getDestination().getState());
            row.createCell(5).setCellValue(trip.getStatus().toString());
            row.createCell(6).setCellValue(trip.getCost());
            row.createCell(7).setCellValue(trip.getDate());

            count++;
        }

        return count;
    }

    private void writeTripsToPdfDocument(List<UsersTrip> trips, Document pdfDocument) throws DocumentException {
        for (UsersTrip trip : trips) {
            Paragraph id = new Paragraph("Id: " + trip.getId());
            Paragraph originCity = new Paragraph("Origin City: " + trip.getOrigin().getCity());
            Paragraph originState = new Paragraph("Origin State: " + trip.getOrigin().getState());
            Paragraph destinationCity = new Paragraph("Destination City: " + trip.getDestination().getCity());
            Paragraph destinationState = new Paragraph("Destination State: " + trip.getDestination().getState());
            Paragraph status = new Paragraph("Status: " + trip.getStatus().toString());
            Paragraph date = new Paragraph("Date: " + trip.getDate());
            Paragraph cost = new Paragraph("Cost: " + trip.getCost());
            Paragraph emptyLine = new Paragraph(" ");

            pdfDocument.add(id);
            pdfDocument.add(originCity);
            pdfDocument.add(originState);
            pdfDocument.add(destinationCity);
            pdfDocument.add(destinationState);
            pdfDocument.add(status);
            pdfDocument.add(date);
            pdfDocument.add(cost);
            pdfDocument.add(emptyLine);
        }
    }

    private void writeLinesToWordDocument(XWPFDocument wordDocument, List<UsersTrip> trips) {
        for (UsersTrip trip : trips) {
            XWPFParagraph paragraph = wordDocument.createParagraph();
            XWPFRun run = paragraph.createRun();
            run.setText("Id: " + trip.getId() + "\n");
            run.setText("Origin City: " + trip.getOrigin().getCity() + "\n");
            run.setText("Origin State: " + trip.getOrigin().getState() + "\n");
            run.setText("Destination City: " + trip.getDestination().getCity() + "\n");
            run.setText("Destination State: " + trip.getDestination().getState() + "\n");
            run.setText("Status: " + trip.getStatus().toString() + "\n");
            run.setText("Date: " + trip.getDate() + "\n");
            run.setText("Cost: " + trip.getCost() + "\n");
            run.setText("\n");
        }
    }
}
