package com.geekhub.palfortrip.service;

import com.geekhub.palfortrip.dto.*;
import com.geekhub.palfortrip.model.Route;
import com.geekhub.palfortrip.model.Trip;
import com.geekhub.palfortrip.model.User;
import com.geekhub.palfortrip.repository.city.CityRepository;
import com.geekhub.palfortrip.repository.feedback.FeedbackRepository;
import com.geekhub.palfortrip.repository.route.RouteRepository;
import com.geekhub.palfortrip.repository.trip.TripRepository;
import com.geekhub.palfortrip.repository.user.UserRepository;
import com.geekhub.palfortrip.utils.PalForTripUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.geekhub.palfortrip.utils.PalForTripUtils.cityFromFormInput;
import static com.geekhub.palfortrip.utils.PalForTripUtils.dateFromInput;

@Service
public class TripService {

    private TripRepository tripRepository;
    private UserRepository userRepository;
    private CityRepository cityRepository;
    private RouteRepository routeRepository;
    private FeedbackRepository feedbackRepository;

    public TripService(TripRepository tripRepository, UserRepository userRepository, CityRepository cityRepository, RouteRepository routeRepository, FeedbackRepository feedbackRepository) {
        this.tripRepository = tripRepository;
        this.userRepository = userRepository;
        this.cityRepository = cityRepository;
        this.routeRepository = routeRepository;
        this.feedbackRepository = feedbackRepository;
    }

    @Transactional
    public Integer saveNewTrip(TripSuggestionForm form) {
        String currentUserName = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findUserByUsername(currentUserName);

        Trip trip = Trip.tripFromForm(form);
        trip.setAuthorId(user.getId());
        trip = tripRepository.saveTrip(trip);
        List<Route> routes = createRoutes(form, trip.getId());
        routeRepository.saveRoutes(routes);
        ArrayList<City> tripPoints = citiesFromForm(form);
        cityRepository.saveTripPoints(tripPoints, trip.getId());
        return trip.getId();
    }

    public List<City> findCitiesStartsFrom(String string) {
        return cityRepository.findCitiesStartsFrom(string);
    }

    public List<TripDto> findTrips(FindTripForm form) {
        FindTripDto findTripDto = new FindTripDto();
        findTripDto.setOrigin(cityFromFormInput(form.getFrom()));
        findTripDto.setDestination(cityFromFormInput(form.getTo()));
        findTripDto.setDate(dateFromInput(form.getDate()));
        return tripRepository.findTrips(findTripDto);
    }

    @Transactional
    public TripDetails getTripById(Integer id) {
        TripDetails trip = tripRepository.getTripById(id);
        List<City> routePoints = routeRepository.getRoutePointsByTripId(id);
        trip.setTripRoutePoints(routePoints);
        Integer authorAverageRating = feedbackRepository.getAverageRatingForUser(trip.getAuthorUsername());
        trip.setAuthorRating(authorAverageRating);
        return trip;
    }

    @Transactional
    public Map<String, List<UsersTrip>> getTripsByUsernameGroupByStatus(String username) {
        User user = userRepository.findUserByUsername(username);
        List<UsersTrip> usersTrips = tripRepository.getTripsByUserId(user.getId());
        return usersTrips.stream().collect(Collectors.groupingBy(ut -> ut.getStatus().toString()));
    }

    public void completeTrip(Integer tripId) {
        tripRepository.completeTrip(tripId);
    }

    public void cancelTrip(Integer tripId) {
        tripRepository.cancelTrip(tripId);
    }

    private List<Route> createRoutes(TripSuggestionForm form, Integer tripId) {
        List<Route> possibleRoutes = new ArrayList<>();

        ArrayList<City> citiesFromForm = citiesFromForm(form);
        for (int i = 0; i < citiesFromForm.size(); i++) {
            for (int j = i + 1; j < citiesFromForm.size(); j++) {
                Route route = new Route();
                route.setOriginCity(citiesFromForm.get(i));
                route.setDestinationCity(citiesFromForm.get(j));
                route.setTripId(tripId);
                possibleRoutes.add(route);
            }
        }

        return possibleRoutes;
    }

    private ArrayList<City> citiesFromForm(TripSuggestionForm form) {
        ArrayList<City> cities = new ArrayList<>();
        City origin = cityFromFormInput(form.getOrigin());
        cities.add(origin);

        List<City> intermediatePoints = form.getIntermediatePoints().stream()
                .filter(p -> !p.equals(""))
                .map(PalForTripUtils::cityFromFormInput)
                .collect(Collectors.toList());
        cities.addAll(intermediatePoints);

        City destination = cityFromFormInput(form.getDestination());
        cities.add(destination);
        return cities;
    }
}
