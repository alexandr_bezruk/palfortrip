package com.geekhub.palfortrip.service;

import com.geekhub.palfortrip.dto.RegistrationForm;
import com.geekhub.palfortrip.model.Role;
import com.geekhub.palfortrip.model.User;
import com.geekhub.palfortrip.repository.role.RoleRepository;
import com.geekhub.palfortrip.repository.user.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.HashSet;

@Service
public class UserService {

    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Boolean userExists(String username) {
        return userRepository.isUserExists(username);
    }

    @Transactional
    public void saveUser(RegistrationForm form) {
        User user = new User(form);
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        Role role = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<>(Collections.singletonList(role)));
        user = userRepository.saveNewUser(user);
        roleRepository.setRolesForUser(user);
    }

    @Transactional
    public User findUserByUsername(String username) {
        User user = userRepository.findUserByUsername(username);
        user.setRoles(roleRepository.getRolesForUser(user));
        return user;
    }

    public Boolean isUserExists(String username) {
        return userRepository.isUserExists(username);
    }
}
