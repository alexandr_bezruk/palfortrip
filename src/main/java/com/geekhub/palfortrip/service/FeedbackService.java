package com.geekhub.palfortrip.service;

import com.geekhub.palfortrip.model.User;
import com.geekhub.palfortrip.dto.Feedback;
import com.geekhub.palfortrip.dto.FeedbackForm;
import com.geekhub.palfortrip.repository.user.UserRepository;
import com.geekhub.palfortrip.repository.feedback.FeedbackRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FeedbackService {

    private FeedbackRepository feedbackRepository;
    private UserRepository userRepository;

    public FeedbackService(FeedbackRepository feedbackRepository, UserRepository userRepository) {
        this.feedbackRepository = feedbackRepository;
        this.userRepository = userRepository;
    }

    @Transactional
    public void saveFeedback(FeedbackForm form) {
        String currentUserName = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = userRepository.findUserByUsername(currentUserName);

        form.setAuthorId(user.getId());
        feedbackRepository.saveFeedback(form);
    }

    public List<Feedback> getFeedbacksForUser(String username) {
        return feedbackRepository.getFeedbacksForUser(username);
    }
}
