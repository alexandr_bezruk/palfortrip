package com.geekhub.palfortrip.dto;

import com.geekhub.palfortrip.validation.FieldMatch;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@FieldMatch(first = "password", second = "passwordConfirmation", message = "The password fields must match")
public class RegistrationForm {

    @NotEmpty(message = "Please, supply login")
    private String username;

    @NotEmpty(message = "Please, supply phone number in correct format")
    @Pattern(regexp = "\\+?[1-9]\\d{1,14}", message = "Please, supply phone number in appropiate format.\n" +
            "For example: +380331111111")
    private String phoneNumber;

    @NotEmpty(message = "Please, supply first name")
    private String firstName;

    @NotEmpty(message = "Please, supply last name")
    private String lastName;

    @NotEmpty(message = "Please, supply password")
    @Size(min = 8, message = "Password should be at least 8 characters long")
    private String password;

    @Size(min = 8, message = "Password Confirmation should be at least 8 characters long")
    private String passwordConfirmation;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
