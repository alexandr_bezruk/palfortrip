package com.geekhub.palfortrip.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class FindTripForm {

    @NotEmpty(message = "Please supply origin")
    @Pattern(regexp = "[A-Z][A-Za-z]*,\\s[A-Z][A-Za-z]*", message = "Please, select appropriate" +
            " value from drop down list")
    private String from;

    @NotEmpty(message = "Please supply origin")
    @Pattern(regexp = "[A-Z][A-Za-z]*,\\s[A-Z][A-Za-z]*", message = "Please, select appropriate" +
            " value from drop down list")
    private String to;

    @NotEmpty(message = "Please supply date")
    @Pattern(regexp = "\\d{2}/\\d{2}/\\d{4}", message = "Please supply date in format: dd/mm/yyyy")
    private String date;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
