package com.geekhub.palfortrip.dto;

import java.time.LocalDate;

public class FindTripDto {

    private City origin;
    private City destination;
    private LocalDate date;

    public City getOrigin() {
        return origin;
    }

    public void setOrigin(City origin) {
        this.origin = origin;
    }

    public City getDestination() {
        return destination;
    }

    public void setDestination(City destination) {
        this.destination = destination;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
