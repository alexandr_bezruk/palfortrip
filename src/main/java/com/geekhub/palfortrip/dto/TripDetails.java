package com.geekhub.palfortrip.dto;

import com.geekhub.palfortrip.model.TripStatus;

import java.util.List;

public class TripDetails {

    private Integer id;
    private String authorFullName;
    private String authorUsername;
    private Integer authorProfilePictureId;
    private Integer authorRating;
    private TripStatus status;
    private Double cost;
    private String date;
    private List<City> tripRoutePoints;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAuthorFullName() {
        return authorFullName;
    }

    public void setAuthorFullName(String authorFullName) {
        this.authorFullName = authorFullName;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }

    public Integer getAuthorProfilePictureId() {
        return authorProfilePictureId;
    }

    public void setAuthorProfilePictureId(Integer authorProfilePictureId) {
        this.authorProfilePictureId = authorProfilePictureId;
    }

    public Integer getAuthorRating() {
        return authorRating;
    }

    public void setAuthorRating(Integer authorRating) {
        this.authorRating = authorRating;
    }

    public TripStatus getStatus() {
        return status;
    }

    public void setStatus(TripStatus status) {
        this.status = status;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<City> getTripRoutePoints() {
        return tripRoutePoints;
    }

    public void setTripRoutePoints(List<City> tripRoutePoints) {
        this.tripRoutePoints = tripRoutePoints;
    }
}
