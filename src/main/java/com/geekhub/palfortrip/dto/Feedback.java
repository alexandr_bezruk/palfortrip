package com.geekhub.palfortrip.dto;

public class Feedback {

    private String authorFullName;
    private String authorUsername;
    private Integer authorProfilePictureId;
    private String date;
    private String message;
    private Integer rating;

    public String getAuthorFullName() {
        return authorFullName;
    }

    public void setAuthorFullName(String authorFullName) {
        this.authorFullName = authorFullName;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public Integer getAuthorProfilePictureId() {
        return authorProfilePictureId;
    }

    public void setAuthorProfilePictureId(Integer authorProfilePictureId) {
        this.authorProfilePictureId = authorProfilePictureId;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}