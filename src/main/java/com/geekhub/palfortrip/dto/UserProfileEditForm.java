package com.geekhub.palfortrip.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class UserProfileEditForm {

    private String username;

    @NotEmpty(message = "Please, supply first name")
    private String firstName;

    @NotEmpty(message = "Please, supply phone number")
    @Pattern(regexp = "\\+?[1-9]\\d{1,14}", message = "Please, supply phone number in appropiate format.\n" +
            "For example: +380331111111")
    private String phoneNumber;

    @NotEmpty(message = "Please, supply last name")
    private String lastName;
    private Integer pictureId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getPictureId() {
        return pictureId;
    }

    public void setPictureId(Integer pictureId) {
        this.pictureId = pictureId;
    }
}
