package com.geekhub.palfortrip.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

public class TripSuggestionForm {

    @NotEmpty(message = "Please supply origin")
    @Pattern(regexp = "[A-Z][A-Za-z]*,\\s[A-Z][A-Za-z]*", message = "Please, select appropriate" +
            " value from drop down list")
    private String origin;

    @NotEmpty(message = "Please supply destination")
    @Pattern(regexp = "[A-Z][A-Za-z]*,\\s[A-Z][A-Za-z]*", message = "Please, select appropriate" +
            " value from drop down list")
    private String destination;

    @NotEmpty(message = "Please supply cost")
    @Digits(fraction = 0, integer = 5, message = "You can use only digits")
    private Double cost;

    @NotEmpty(message = "Please supply date")
    @Pattern(regexp = "\\d{2}/\\d{2}/\\d{4}", message = "Please supply date in format: dd/mm/yyyy")
    private String date;
    private String hours;
    private String minutes;

    private List<@Pattern(regexp = "([A-Z][A-Za-z]*,\\s[A-Z][A-Za-z]*)?",
            message = "Please, select appropriate" +
            " value from drop down list") String> intermediatePoints;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String from) {
        this.origin = from;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public List<String> getIntermediatePoints() {
        return intermediatePoints;
    }

    public void setIntermediatePoints(List<String> intermediatePoint) {
        this.intermediatePoints = intermediatePoint;
    }
}
