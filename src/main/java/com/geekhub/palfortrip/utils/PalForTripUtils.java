package com.geekhub.palfortrip.utils;

import com.geekhub.palfortrip.dto.City;
import com.geekhub.palfortrip.dto.TripSuggestionForm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PalForTripUtils {

    private PalForTripUtils() {

    }

    public static String getStringDate(ResultSet rs) throws SQLException {
        Timestamp date = rs.getTimestamp("date");
        LocalDateTime dateTime = date.toLocalDateTime();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTime.format(formatter);
    }

    public static String getAuthorFullName(ResultSet rs) throws SQLException {
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");

        return firstName + " " + lastName;
    }

    public static City cityFromFormInput(String formInput) {
        String cityName = formInput.substring(0, formInput.indexOf(","));
        String state = formInput.substring(formInput.indexOf(",") + 2);

        City city = new City();
        city.setCity(cityName);
        city.setState(state);

        return city;
    }

    public static LocalDateTime dateTimeFromForm(TripSuggestionForm form) {
        String dateAndTime = form.getDate() + " " + form.getHours() + ":" + form.getMinutes() + ":00";
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
        return LocalDateTime.parse(dateAndTime, dateTimeFormatter);
    }

    public static LocalDate dateFromInput(String formInput) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        return LocalDate.parse(formInput, formatter);
    }
}
