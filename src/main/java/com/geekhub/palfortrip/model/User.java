package com.geekhub.palfortrip.model;

import com.geekhub.palfortrip.dto.RegistrationForm;

import java.util.Set;

public class User {

    private Integer id;
    private String username;
    private String phoneNumber;
    private String firstName;
    private String lastName;
    private String password;
    private Set<Role> roles;

    public User() {

    }

    public User(RegistrationForm registrationForm) {
        this.username = registrationForm.getUsername();
        this.phoneNumber = registrationForm.getPhoneNumber();
        this.firstName = registrationForm.getFirstName();
        this.lastName = registrationForm.getLastName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String login) {
        this.username = login;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
