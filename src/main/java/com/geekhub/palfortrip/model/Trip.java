package com.geekhub.palfortrip.model;

import com.geekhub.palfortrip.dto.City;
import com.geekhub.palfortrip.dto.TripSuggestionForm;
import com.geekhub.palfortrip.utils.PalForTripUtils;

import java.time.LocalDateTime;

public class Trip {

    private Integer id;
    private City origin;
    private City destination;
    private Double cost;
    private LocalDateTime dateTime;
    private Integer authorId;

    private Trip() {

    }

    public static Trip tripFromForm(TripSuggestionForm form) {
        Trip trip = new Trip();
        trip.origin = PalForTripUtils.cityFromFormInput(form.getOrigin());
        trip.destination = PalForTripUtils.cityFromFormInput(form.getDestination());
        trip.cost = form.getCost();
        trip.dateTime = PalForTripUtils.dateTimeFromForm(form);
        return trip;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public City getOrigin() {
        return origin;
    }

    public City getDestination() {
        return destination;
    }

    public Double getCost() {
        return cost;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }
}
