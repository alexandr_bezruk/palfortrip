package com.geekhub.palfortrip.model;

public enum TripStatus {

    ACTIVE, COMPLETED;
}
