package com.geekhub.palfortrip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PalForTripApplication {

	public static void main(String[] args) {
		SpringApplication.run(PalForTripApplication.class, args);
	}
}
