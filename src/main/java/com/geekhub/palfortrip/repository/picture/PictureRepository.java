package com.geekhub.palfortrip.repository.picture;

import org.springframework.web.multipart.MultipartFile;

public interface PictureRepository {

    void savePictureForUser(MultipartFile picture, String username);

    byte[] getProfilePictureForUser(String username);

    Boolean userHasProfilePicture(String username);

    void updateProfilePicture(MultipartFile picture, String username);

    Integer getProfilePictureId(String username);
}
