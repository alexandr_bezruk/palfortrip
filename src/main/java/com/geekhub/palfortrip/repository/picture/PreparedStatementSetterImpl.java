package com.geekhub.palfortrip.repository.picture;

import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementSetterImpl implements PreparedStatementSetter {

    private MultipartFile file;
    private String username;

    public PreparedStatementSetterImpl(MultipartFile file, String username) {
        this.file = file;
        this.username = username;
    }

    @Override
    public void setValues(PreparedStatement ps) throws SQLException {
        ps.setString(1, file.getOriginalFilename());
        try {
            ps.setBinaryStream(2, file.getInputStream(), file.getSize());
            ps.setString(3, username);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save file" + file.getOriginalFilename(), e);
        }
    }
}
