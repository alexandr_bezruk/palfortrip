package com.geekhub.palfortrip.repository.picture;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

@Repository
public class PictureRepositoryImpl implements PictureRepository {

    private JdbcTemplate jdbcTemplate;

    public PictureRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void savePictureForUser(MultipartFile picture, String username) {
        String sql = "INSERT INTO profile_pictures (picture_name, picture, username)" +
                " VALUES(?, ?, ?)";

        jdbcTemplate.update(sql, new PreparedStatementSetterImpl(picture, username));
    }

    @Override
    public byte[] getProfilePictureForUser(String username) {
        String sql = "SELECT i.picture FROM profile_pictures i" +
                " JOIN users u ON u.profile_picture_id = i.id WHERE u.username = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{username}, new PictureRowMapper());
    }

    @Override
    public Boolean userHasProfilePicture(String username) {
        String sql = "SELECT EXISTS (SELECT id FROM profile_pictures WHERE username = ?)";

        return jdbcTemplate.queryForObject(sql, new Object[]{username}, Boolean.class);
    }

    @Override
    public void updateProfilePicture(MultipartFile picture, String username) {
        String sql = "UPDATE profile_pictures SET picture_name = ?, picture = ? WHERE username = ?";

        jdbcTemplate.update(sql, new PreparedStatementSetterImpl(picture, username));
    }

    @Override
    public Integer getProfilePictureId(String username) {
        String sql = "SELECT id FROM profile_pictures WHERE username = ?";

        return jdbcTemplate.queryForObject(sql, new Object[]{username}, Integer.class);
    }
}
