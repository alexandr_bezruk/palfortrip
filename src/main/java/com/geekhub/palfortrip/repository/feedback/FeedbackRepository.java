package com.geekhub.palfortrip.repository.feedback;

import com.geekhub.palfortrip.dto.Feedback;
import com.geekhub.palfortrip.dto.FeedbackForm;

import java.util.List;

public interface FeedbackRepository {

    void saveFeedback(FeedbackForm form);

    List<Feedback> getFeedbacksForUser(String username);

    Integer getAverageRatingForUser(String username);
}
