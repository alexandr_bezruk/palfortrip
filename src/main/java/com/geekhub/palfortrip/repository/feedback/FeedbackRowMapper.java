package com.geekhub.palfortrip.repository.feedback;

import com.geekhub.palfortrip.dto.Feedback;
import com.geekhub.palfortrip.utils.PalForTripUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FeedbackRowMapper implements RowMapper<Feedback> {

    @Override
    public Feedback mapRow(ResultSet rs, int rowNum) throws SQLException {
        Feedback feedback = new Feedback();
        feedback.setRating(rs.getInt("rating"));
        feedback.setMessage(rs.getString("message"));
        feedback.setDate(PalForTripUtils.getStringDate(rs));
        feedback.setAuthorFullName(PalForTripUtils.getAuthorFullName(rs));
        feedback.setAuthorUsername(rs.getString("username"));
        feedback.setAuthorProfilePictureId(rs.getInt("profile_picture_id"));

        return feedback;
    }
}
