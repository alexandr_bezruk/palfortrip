package com.geekhub.palfortrip.repository.feedback;

import com.geekhub.palfortrip.dto.Feedback;
import com.geekhub.palfortrip.dto.FeedbackForm;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public class FeedbackRepositoryImpl implements FeedbackRepository {

    NamedParameterJdbcTemplate jdbcTemplate;

    public FeedbackRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void saveFeedback(FeedbackForm form) {
        String sql = "INSERT INTO feedbacks (rating, message, date, target_username, author_id)" +
                " VALUES(:rating, :message, :date, :target_username, :author_id)";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("rating", form.getRating());
        parameterSource.addValue("message", form.getMessage());
        parameterSource.addValue("date", Timestamp.valueOf(form.getDate()));
        parameterSource.addValue("target_username", form.getTargetUsername());
        parameterSource.addValue("author_id", form.getAuthorId());

        jdbcTemplate.update(sql, parameterSource);
    }

    @Override
    public List<Feedback> getFeedbacksForUser(String username) {
        String sql = "SELECT f.rating, f.message, f.date, u.first_name, u.last_name, u.username, u.profile_picture_id" +
                " FROM feedbacks f JOIN users u ON f.author_id = u.id" +
                " WHERE f.target_username = :username";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("username", username);

        return jdbcTemplate.query(sql, parameterSource, new FeedbackRowMapper());
    }

    @Override
    public Integer getAverageRatingForUser(String username) {
        String sql = "SELECT avg(f.rating) FROM feedbacks f WHERE target_username = :username";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("username", username);

        Integer rating = jdbcTemplate.queryForObject(sql, parameterSource, Integer.class);
        return rating == null ? 0 : rating;
    }
}
