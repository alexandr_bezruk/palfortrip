package com.geekhub.palfortrip.repository.route;

import com.geekhub.palfortrip.dto.City;
import com.geekhub.palfortrip.model.Route;
import com.geekhub.palfortrip.repository.city.CityRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RouteRepositoryImpl implements RouteRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public RouteRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void saveRoutes(List<Route> routes) {
        String sql = "INSERT INTO routes (origin_city, origin_state, destination_city, destination_state, trip_id)" +
                " VALUES (:origin_city, :origin_state, :destination_city, :destination_state, :trip_id)";

        MapSqlParameterSource[] parameterSources = createParameterSourcesForBatch(routes);
        jdbcTemplate.batchUpdate(sql, parameterSources);
    }

    @Override
    public List<City> getRoutePointsByTripId(Integer id) {
        String sql = "SELECT city_name AS city, state_name AS state_name, point_order FROM trip_points" +
                " WHERE trip_id = :trip_id" +
                " GROUP BY city_name, state_name, point_order" +
                " ORDER BY point_order";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("trip_id", id);

        return jdbcTemplate.query(sql, parameterSource, new CityRowMapper());
    }

    private MapSqlParameterSource[] createParameterSourcesForBatch(List<Route> routes) {
        MapSqlParameterSource[] parameterSource = new MapSqlParameterSource[routes.size()];
        int count = 0;
        for (Route route : routes) {
            City origin = route.getOriginCity();
            City destination = route.getDestinationCity();
            parameterSource[count] = new MapSqlParameterSource();
            parameterSource[count].addValue("origin_city", origin.getCity());
            parameterSource[count].addValue("origin_state", origin.getState());
            parameterSource[count].addValue("destination_city", destination.getCity());
            parameterSource[count].addValue("destination_state", destination.getState());
            parameterSource[count].addValue("trip_id", route.getTripId());
            count++;
        }

        return parameterSource;
    }
}
