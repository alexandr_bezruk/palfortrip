package com.geekhub.palfortrip.repository.route;

import com.geekhub.palfortrip.dto.City;
import com.geekhub.palfortrip.model.Route;

import java.util.List;

public interface RouteRepository {

    void saveRoutes(List<Route> routes);

    List<City> getRoutePointsByTripId(Integer id);
}
