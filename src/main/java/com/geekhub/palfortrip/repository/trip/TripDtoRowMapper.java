package com.geekhub.palfortrip.repository.trip;

import com.geekhub.palfortrip.dto.City;
import com.geekhub.palfortrip.dto.TripDto;
import com.geekhub.palfortrip.utils.PalForTripUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TripDtoRowMapper implements RowMapper<TripDto> {

    @Override
    public TripDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        TripDto tripDto = new TripDto();

        tripDto.setTripId(rs.getInt("trip_id"));
        City origin = new City();
        origin.setCity(rs.getString("origin_city"));
        origin.setState(rs.getString("origin_state"));
        tripDto.setOrigin(origin);
        City destination = new City();
        destination.setCity(rs.getString("destination_city"));
        destination.setState(rs.getString("destination_state"));
        tripDto.setDestination(destination);
        tripDto.setAuthorFirstName(rs.getString("first_name"));
        tripDto.setAuthorLastName(rs.getString("last_name"));
        tripDto.setDate(PalForTripUtils.getStringDate(rs));
        tripDto.setCost(rs.getDouble("cost"));
        tripDto.setAuthorProfilePictureId(rs.getInt("profile_picture_id"));
        tripDto.setAuthorUsername(rs.getString("username"));

        return tripDto;
    }
}
