package com.geekhub.palfortrip.repository.trip;

import com.geekhub.palfortrip.dto.*;
import com.geekhub.palfortrip.model.Route;
import com.geekhub.palfortrip.model.Trip;

import java.util.List;

public interface TripRepository {

    Trip saveTrip(Trip trip);

    List<TripDto> findTrips(FindTripDto findTripDto);

    TripDetails getTripById(Integer id);

    List<UsersTrip> getTripsByUserId(Integer userId);

    void completeTrip(Integer tripId);

    void cancelTrip(Integer tripId);
}
