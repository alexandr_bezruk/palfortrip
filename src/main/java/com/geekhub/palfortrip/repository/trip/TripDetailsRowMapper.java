package com.geekhub.palfortrip.repository.trip;

import com.geekhub.palfortrip.dto.TripDetails;
import com.geekhub.palfortrip.model.TripStatus;
import com.geekhub.palfortrip.utils.PalForTripUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TripDetailsRowMapper implements RowMapper<TripDetails> {

    @Override
    public TripDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
        TripDetails tripDetails = new TripDetails();

        tripDetails.setId(rs.getInt("id"));
        tripDetails.setAuthorFullName(PalForTripUtils.getAuthorFullName(rs));
        tripDetails.setAuthorProfilePictureId(rs.getInt("profile_picture_id"));
        tripDetails.setAuthorUsername(rs.getString("username"));
        tripDetails.setStatus(TripStatus.valueOf(rs.getString("status")));
        tripDetails.setCost(rs.getDouble("cost"));
        tripDetails.setDate(PalForTripUtils.getStringDate(rs));

        return tripDetails;
    }
}
