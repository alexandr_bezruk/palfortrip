package com.geekhub.palfortrip.repository.trip;

import com.geekhub.palfortrip.dto.City;
import com.geekhub.palfortrip.dto.UsersTrip;
import com.geekhub.palfortrip.model.TripStatus;
import com.geekhub.palfortrip.utils.PalForTripUtils;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersTripRowMapper implements RowMapper<UsersTrip> {

    @Override
    public UsersTrip mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsersTrip usersTrip = new UsersTrip();

        City origin = new City();
        origin.setCity(rs.getString("origin_city"));
        origin.setState(rs.getString("origin_state"));
        City destination = new City();
        destination.setCity(rs.getString("destination_city"));
        destination.setState(rs.getString("destination_state"));
        usersTrip.setOrigin(origin);
        usersTrip.setDestination(destination);
        usersTrip.setId(rs.getInt("id"));
        usersTrip.setCost(rs.getDouble("cost"));
        usersTrip.setDate(PalForTripUtils.getStringDate(rs));
        usersTrip.setStatus(TripStatus.valueOf(rs.getString("status")));

        return usersTrip;
    }
}
