package com.geekhub.palfortrip.repository.trip;

import com.geekhub.palfortrip.dto.*;
import com.geekhub.palfortrip.model.Trip;
import com.geekhub.palfortrip.model.TripStatus;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Repository
public class TripRepositoryImpl implements TripRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public TripRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Trip saveTrip(Trip trip) {
        String sql = "INSERT INTO trips (cost, date, author_id," +
                " origin_city, origin_state, destination_city, destination_state)" +
                " VALUES (:cost, :date, :author_id, :origin_city," +
                " :origin_state, :destination_city, :destination_state)";
        //TODO GET LIST OF CURRENT TRIPS AND THEN LIST OF COMPLETED TRIPS
        //TODO remove join button add download file feature validation complete and cancel trip if will have time refactor.
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("cost", trip.getCost());
        parameterSource.addValue("date", Timestamp.valueOf(trip.getDateTime()));
        parameterSource.addValue("author_id", trip.getAuthorId());
        parameterSource.addValue("origin_city", trip.getOrigin().getCity());
        parameterSource.addValue("origin_state", trip.getOrigin().getState());
        parameterSource.addValue("destination_city", trip.getDestination().getCity());
        parameterSource.addValue("destination_state", trip.getDestination().getState());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, parameterSource, keyHolder, new String[]{"id"});

        int generatedId = keyHolder.getKey().intValue();
        trip.setId(generatedId);

        return trip;
    }

    @Override
    public List<TripDto> findTrips(FindTripDto findTripDto) {

        String sql = "SELECT r.origin_city, r.origin_state, r.destination_city, r.destination_state," +
                " u.first_name, u.last_name, u.username, u.profile_picture_id, t.cost, t.date, t.id AS trip_id" +
                " FROM routes r" +
                " JOIN trips t ON r.trip_id = t.id" +
                " JOIN users u ON t.author_id = u.id " +
                " WHERE r.origin_city = :origin_city AND r.origin_state = :origin_state" +
                " AND r.destination_city = :destination_city" +
                " AND r.destination_state = :destination_state" +
                " AND t.date > :date" +
                " AND status = 'ACTIVE'";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        City origin = findTripDto.getOrigin();
        City destination = findTripDto.getDestination();
        parameterSource.addValue("origin_city", origin.getCity());
        parameterSource.addValue("origin_state", origin.getState());
        parameterSource.addValue("destination_city", destination.getCity());
        parameterSource.addValue("destination_state", destination.getState());
        parameterSource.addValue("date", findTripDto.getDate());

        return jdbcTemplate.query(sql, parameterSource, new TripDtoRowMapper());
    }

    @Override
    public TripDetails getTripById(Integer id) {
        String sql = "SELECT u.first_name, u.last_name, u.username, u.profile_picture_id," +
                " t.id, t.cost, t.date, t.status" +
                " FROM trips t JOIN users u ON t.author_id = u.id WHERE t.id = :id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", id);

        return jdbcTemplate.queryForObject(sql, parameterSource, new TripDetailsRowMapper());
    }

    @Override
    public List<UsersTrip> getTripsByUserId(Integer userId) {
        String sql = "SELECT t.origin_city, t.origin_state, t.destination_city," +
                " t.destination_state, t.id, t.cost, t.date, t.status" +
                " FROM trips t WHERE author_id = :author_id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("author_id", userId);

        return jdbcTemplate.query(sql, parameterSource, new UsersTripRowMapper());
    }

    @Override
    public void completeTrip(Integer tripId) {
        String sql = "UPDATE trips SET status = :status WHERE id = :id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("status", TripStatus.COMPLETED.toString());
        parameterSource.addValue("id", tripId);

        jdbcTemplate.update(sql, parameterSource);
    }

    @Transactional
    @Override
    public void cancelTrip(Integer tripId) {
        deleteRoutesByTripId(tripId);
        deleteTripPointsByTripId(tripId);
        deleteTripById(tripId);
    }

    private void deleteRoutesByTripId(Integer tripId) {
        String sql = "DELETE FROM routes WHERE trip_id = :id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", tripId);

        jdbcTemplate.update(sql, parameterSource);
    }

    private void deleteTripPointsByTripId(Integer tripId) {
        String sql = "DELETE FROM trip_points WHERE trip_id = :id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", tripId);

        jdbcTemplate.update(sql, parameterSource);
    }

    private void deleteTripById(Integer tripId) {
        String sql = "DELETE FROM trips WHERE id = :id";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("id", tripId);

        jdbcTemplate.update(sql, parameterSource);
    }
}
