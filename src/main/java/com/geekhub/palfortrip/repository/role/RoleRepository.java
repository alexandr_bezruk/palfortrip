package com.geekhub.palfortrip.repository.role;

import com.geekhub.palfortrip.model.Role;
import com.geekhub.palfortrip.model.User;

import java.util.Set;

public interface RoleRepository {

    Role findByRole(String role);

    void setRolesForUser(User user);

    Set<Role> getRolesForUser(User user);
}
