package com.geekhub.palfortrip.repository.role;

import com.geekhub.palfortrip.model.Role;
import com.geekhub.palfortrip.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public RoleRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Role findByRole(String role) {
        String sql = "SELECT r.id, r.role FROM roles r WHERE role = :role";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("role", role);
        return jdbcTemplate.queryForObject(sql, parameterSource, new RoleRowMapper());
    }

    @Override
    @Transactional
    public void setRolesForUser(User user) {
        String sql = "INSERT INTO users_roles (user_id, role_id)" +
                " VALUES (:userId, :roleId)";
        for (Role role : user.getRoles()) {
            MapSqlParameterSource parameterSource = new MapSqlParameterSource();
            parameterSource.addValue("userId", user.getId());
            parameterSource.addValue("roleId", role.getId());
            jdbcTemplate.update(sql, parameterSource);
        }
    }

    @Override
    public Set<Role> getRolesForUser(User user) {
        String sql = "SELECT r.id, r.role FROM roles r" +
                " JOIN users_roles ON r.id = users_roles.role_id" +
                " WHERE user_id = :userId";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("userId", user.getId());

        return new HashSet<>(jdbcTemplate.query(sql, parameterSource, new RoleRowMapper()));
    }
}
