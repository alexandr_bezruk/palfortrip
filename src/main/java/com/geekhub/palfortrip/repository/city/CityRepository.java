package com.geekhub.palfortrip.repository.city;

import com.geekhub.palfortrip.dto.City;

import java.util.List;

public interface CityRepository {

    List<City> findCitiesStartsFrom(String string);

    void saveTripPoints(List<City> tripPoints, Integer tripId);

    List<City> getTripRoutePoints(Integer tripId);
}
