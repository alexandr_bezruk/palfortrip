package com.geekhub.palfortrip.repository.city;

import com.geekhub.palfortrip.dto.City;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityRowMapper implements RowMapper<City> {

    @Override
    public City mapRow(ResultSet rs, int rowNum) throws SQLException {
        City city = new City();

        city.setCity(rs.getString("city"));
        city.setState(rs.getString("state_name"));

        return city;
    }
}
