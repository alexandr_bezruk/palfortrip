package com.geekhub.palfortrip.repository.city;

import com.geekhub.palfortrip.dto.City;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CityRepositoryImpl implements CityRepository {

    NamedParameterJdbcTemplate jdbcTemplate;

    public CityRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<City> findCitiesStartsFrom(String string) {
        String sql = "SELECT c.city, s.state_name FROM us_cities c JOIN us_states s" +
                " ON c.id_state = s.id" +
                " WHERE c.city LIKE :string ORDER BY c.city LIMIT 5";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("string", string + "%");
        return jdbcTemplate.query(sql, parameterSource, new CityRowMapper());
    }

    @Override
    public void saveTripPoints(List<City> tripPoints, Integer tripId) {
        String sql = "INSERT INTO trip_points (city_name, state_name, point_order, trip_id)" +
                " VALUES (:city_name, :state_name, :point_order, :trip_id)";

        MapSqlParameterSource[] parameterSource = createParameterSourcesForBatch(tripPoints, tripId);
        jdbcTemplate.batchUpdate(sql, parameterSource);
    }

    @Override
    public List<City> getTripRoutePoints(Integer tripId) {
        String sql = "SELECT tp.city_name as city, tp.state_name as state, tp.point_order" +
                " FROM trip_points tp" +
                " WHERE trip_id = :id" +
                " ORDER BY tp.point_order";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("tripId", tripId);

        return jdbcTemplate.query(sql, parameterSource, new CityRowMapper());
    }

    private MapSqlParameterSource[] createParameterSourcesForBatch(List<City> tripPoints, Integer tripId) {
        MapSqlParameterSource[] parameterSource = new MapSqlParameterSource[tripPoints.size()];
        int count = 0;
        for (City point : tripPoints) {
            parameterSource[count] = new MapSqlParameterSource();
            parameterSource[count].addValue("city_name", point.getCity());
            parameterSource[count].addValue("state_name", point.getState());
            parameterSource[count].addValue("point_order", count);
            parameterSource[count].addValue("trip_id", tripId);
            count++;
        }

        return parameterSource;
    }
}
