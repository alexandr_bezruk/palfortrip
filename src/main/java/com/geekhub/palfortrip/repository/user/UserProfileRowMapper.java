package com.geekhub.palfortrip.repository.user;

import com.geekhub.palfortrip.dto.UserProfile;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserProfileRowMapper implements RowMapper<UserProfile> {

    @Override
    public UserProfile mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserProfile userProfile = new UserProfile();
        userProfile.setUsername(rs.getString("username"));
        userProfile.setPhoneNumber(rs.getString("phone_number"));
        userProfile.setFirstName(rs.getString("first_name"));
        userProfile.setLastName(rs.getString("last_name"));
        userProfile.setPictureId(rs.getInt("profile_picture_id"));

        return userProfile;
    }
}
