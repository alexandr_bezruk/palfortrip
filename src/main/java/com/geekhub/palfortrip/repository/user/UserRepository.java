package com.geekhub.palfortrip.repository.user;

import com.geekhub.palfortrip.dto.UserProfile;
import com.geekhub.palfortrip.dto.UserProfileEditForm;
import com.geekhub.palfortrip.model.User;

public interface UserRepository {

    Boolean isUserExists(String username);

    User findUserByUsername(String username);

    User saveNewUser(User user);

    void updateUser(UserProfileEditForm form);

    UserProfile findUserProfileByUsername(String username);
}
