package com.geekhub.palfortrip.repository.user;

import com.geekhub.palfortrip.dto.UserProfile;
import com.geekhub.palfortrip.dto.UserProfileEditForm;
import com.geekhub.palfortrip.model.User;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    public UserRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Boolean isUserExists(String username) {
        String sql = "SELECT EXISTS (SELECT u.id FROM users u WHERE username = :username)";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource("username", username);

        return jdbcTemplate.queryForObject(sql, parameterSource, Boolean.class);
    }

    @Override
    public User findUserByUsername(String username) {
        String sql = "SELECT u.id, u.username, u.phone_number, u.first_name, u.last_name, u.password" +
                " FROM users u WHERE username = :username";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("username", username);
        return jdbcTemplate.queryForObject(sql, parameterSource, new UserRowMapper());
    }

    @Override
    public User saveNewUser(User user) {
        String sql = "INSERT INTO users (phone_number, username, first_name, last_name, password)" +
                " VALUES (:username, :phone_number, :first_name, :last_name, :password)";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("username", user.getUsername());
        parameterSource.addValue("phone_number", user.getPhoneNumber());
        parameterSource.addValue("first_name", user.getFirstName());
        parameterSource.addValue("last_name", user.getLastName());
        parameterSource.addValue("password", user.getPassword());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, parameterSource, keyHolder, new String[]{"id"});

        Integer generatedId = keyHolder.getKey().intValue();
        user.setId(generatedId);
        return user;
    }

    @Override
    public void updateUser(UserProfileEditForm form) {
        String sql = "UPDATE users " +
                "SET first_name = :first_name, last_name = :last_name, phone_number = :phone_number, profile_picture_id = :picture_id " +
                "WHERE username = :username";

        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("first_name", form.getFirstName());
        parameterSource.addValue("last_name", form.getLastName());
        parameterSource.addValue("username", form.getUsername());
        parameterSource.addValue("phone_number", form.getPhoneNumber());
        parameterSource.addValue("picture_id", form.getPictureId());

        jdbcTemplate.update(sql, parameterSource);
    }

    @Override
    public UserProfile findUserProfileByUsername(String username) {
        String sql = "SELECT u.username, u.phone_number, u.first_name, u.last_name, u.profile_picture_id" +
                " FROM users u WHERE username = :username";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("username", username);
        return jdbcTemplate.queryForObject(sql, parameterSource, new UserProfileRowMapper());
    }
}
