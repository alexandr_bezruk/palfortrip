ALTER TABLE public.trips ADD origin_city VARCHAR(100) NOT NULL;
ALTER TABLE public.trips ADD origin_state VARCHAR(100) NOT NULL;
ALTER TABLE public.trips ADD destination_city VARCHAR(100) NOT NULL;
ALTER TABLE public.trips ADD destination_state VARCHAR(100) NOT NULL;