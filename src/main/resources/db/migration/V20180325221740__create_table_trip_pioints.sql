CREATE TABLE trip_points
(
  city_name VARCHAR(100) NOT NULL,
  state_name VARCHAR(100) NOT NULL,
  point_order INT NOT NULL,
  trip_id INT NOT NULL,
  CONSTRAINT trip_points_users_id_fk FOREIGN KEY (trip_id) REFERENCES trips (id)
);