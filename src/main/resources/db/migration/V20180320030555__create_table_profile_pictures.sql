CREATE TABLE profile_pictures
(
  id SERIAL PRIMARY KEY NOT NULL,
  picture_name VARCHAR(100) NOT NULL,
  picture BYTEA NOT NULL
);