ALTER TABLE profile_pictures ADD username VARCHAR(255) NOT NULL,
  ADD CONSTRAINT profile_pictures_users_username_fk
FOREIGN KEY (username) REFERENCES users (username);