CREATE TABLE feedbacks
(
  id SERIAL PRIMARY KEY NOT NULL,
  rating INT NOT NULL,
  message TEXT NOT NULL,
  date TIMESTAMP NOT NULL,
  target_username VARCHAR(100) NOT NULL,
  author_id INT NOT NULL,
  CONSTRAINT table_name_users_id_fk FOREIGN KEY (author_id) REFERENCES users (id)
);