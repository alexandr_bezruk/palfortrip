CREATE TABLE trips
(
  id        SERIAL PRIMARY KEY NOT NULL,
  cost      DOUBLE PRECISION   NOT NULL,
  date      TIMESTAMP          NOT NULL,
  author_id INT                NOT NULL,
  CONSTRAINT table_name_users_id_fk FOREIGN KEY (author_id) REFERENCES users (id)
);
