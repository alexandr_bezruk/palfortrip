CREATE TABLE routes
(
  id SERIAL PRIMARY KEY NOT NULL,
  origin_city VARCHAR(50) NOT NULL,
  origin_state VARCHAR(50) NOT NULL,
  destination_city VARCHAR(50) NOT NULL,
  destination_state VARCHAR(50) NOT NULL,
  trip_id INT NOT NULL,
  CONSTRAINT routes_trips_id_fk FOREIGN KEY (trip_id) REFERENCES trips (id)
);