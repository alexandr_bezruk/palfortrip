ALTER TABLE public.users ADD profile_picture_id INT NULL;
ALTER TABLE public.users
  ADD CONSTRAINT users_profile_pictures_id_fk
FOREIGN KEY (profile_picture_id) REFERENCES profile_pictures (id);