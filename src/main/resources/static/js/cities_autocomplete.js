$(document).ready(function () {

    $(".autocomplete").autocomplete({
        source: function (request, response) {
            $.getJSON("/findCity", request, function (result) {
                response($.map(result, function (item) {
                    return {
                        label: item.city + ', ' + item.state + ', US',
                        value: item.city + ', ' + item.state
                    }
                }));
            });
        }
    });
});

