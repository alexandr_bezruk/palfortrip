$(document).ready(function () {
    var count = 1;

    $('.add_button').click(function () {
        var kakoi = $(this).attr('fldnum');
        if (count < 3) {
            var insHTML = '<div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i></span><input class="form-control input-lg" type="text" th:field="*{intermediatePoints[' + count + ']}" name="intermediatePoints[' + count + ']">                                            <span th:if="${#fields.hasErrors(\'intermediatePoints['+ count+'\')}" th:errors="*{intermediatePoints[' + count + ']}" class="has-error"></span>\<span class="input-group-btn"><button class="btn btn-lg btn-danger remove_button" type="button"><span class="glyphicon glyphicon-minus"></span></button></span></div>';
            $("#fld" + kakoi).append(insHTML);

            $(".wrp").find('input[type=text]').autocomplete({
                source: function (request, response) {
                    $.getJSON("/findCity", request, function (result) {
                        response($.map(result, function (item) {
                            return {
                                label: item.city + ', ' + item.state + ', US',
                                value: item.city + ', ' + item.state
                            }
                        }));
                    });
                }
            });
            count++;
        }
    });

    $('.fld_wrap').on('click', '.remove_button', function (e) {
        e.preventDefault();
        $(this).parents(':eq(1)').remove();
        count--;
    });
});

